package org.usfirst.frc.team4320.robot.subsystems;

import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.usfirst.frc.team4320.robot.Robot;
import org.usfirst.frc.team4320.robot.RobotMap;
import org.usfirst.frc.team4320.utils.Reportable;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 */
public class Shifters extends Subsystem implements Reportable {

	private DoubleSolenoid leftShifter, rightShifter;
	private Lock writeLock, readLock;
	private long stamp;
	
	public Shifters() {
		leftShifter = new DoubleSolenoid(RobotMap.SHIFTER_LEFT_OPEN, RobotMap.SHIFTER_LEFT_CLOSE);
		rightShifter = new DoubleSolenoid(RobotMap.SHIFTER_RIGHT_OPEN, RobotMap.SHIFTER_RIGHT_CLOSE);
		writeLock = new ReentrantLock();
		readLock = new ReentrantLock();
		stamp = 0;

		setStatus(Value.kReverse);
		
		if (Robot.chassis != null)
			Robot.chassis.updateDistancePerPulse();
	}
	
	public void setStatus(Value value) {
		writeLock.lock();
		readLock.lock();
		try {
			if (Robot.pneumatics != null && Robot.pneumatics.permission()) {
				leftShifter.set(value);
				rightShifter.set(value);
			}
		} finally {
			readLock.unlock();
			writeLock.unlock();
		}
	}
	
	public Value getStatus() {
		readLock.lock();
		try {
			return leftShifter.get();
		} finally {
			readLock.unlock();
		}
	}
	
	public double getRatio() {
		readLock.lock();
		try {
			Value status = leftShifter.get();
			if (status == Value.kForward)
				return RobotMap.HIGH_SHIFTER_RATIO;
			else if (status == Value.kReverse)
				return RobotMap.LOW_SHIFTER_RATIO;
			else
				return 0;
		} finally {
			readLock.unlock();
		}
	}
	
	public long getWriteLock() {
		writeLock.lock();
		while (stamp == 0)
			stamp = new Random().nextLong();
		return stamp;
	}
	
	public void releseWriteLock(long stamp) {
		if (this.stamp != 0 && this.stamp == stamp) {
			this.stamp = 0;
			writeLock.unlock();
		}
	}

	public void reportStatus() {
		String report = "";
		Value status = getStatus();
		
		if (status == Value.kForward)
			report = "On";
		else if (status == Value.kReverse)
			report = "Off";
		else
			report = "No Power";
		
		SmartDashboard.putString("Power Mode", report);
	}
	
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}

