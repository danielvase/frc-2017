package org.usfirst.frc.team4320.robot.subsystems;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.wpilibj.CameraServer;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 */
public class CamerasControl extends Subsystem {

	private UsbCamera frontCamera;
	
	public CamerasControl() {
		frontCamera = null;
	}
	
	public void startCameras() {
		try {
			frontCamera = CameraServer.getInstance().startAutomaticCapture();
		} catch (Exception e) {
			DriverStation.reportError("camera down", false);
		}
	}
	
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}

