package org.usfirst.frc.team4320.robot.subsystems;

import org.usfirst.frc.team4320.robot.RobotMap;
import org.usfirst.frc.team4320.utils.Reportable;

import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 */
public class PneumaticControl extends Subsystem implements Reportable {

	private Compressor compressor;
	private AnalogInput pressureSensor;
	boolean isOn;
	
	public PneumaticControl() {
		compressor = new Compressor();
		pressureSensor = new AnalogInput(RobotMap.PRESSURE_SENSOR);
		isOn = false;
		
		stopCompressor();
	}
	
	public void startCompressor() {
		compressor.start();
		isOn = true;
	}
	
	public void stopCompressor() {
		compressor.stop();
		isOn = false;
	}
	
	public boolean isOn() {
		return isOn;
	}
	
	public int getPressure() {
		return -1;
	}
	
	public boolean permission() {
		return true;
	}
	
	public void reportStatus() {
		SmartDashboard.putNumber("System Pressure", getPressure());
	}

    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}

