package org.usfirst.frc.team4320.robot.subsystems;

import org.usfirst.frc.team4320.robot.RobotMap;
import org.usfirst.frc.team4320.utils.Reportable;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 */
public class GearLifter extends Subsystem implements Reportable {

	private DoubleSolenoid lifter;
	
	public GearLifter() {
		lifter = new DoubleSolenoid(RobotMap.GEAR_LIFTER_OPEN, RobotMap.GEAR_LIFTER_CLOSE);
		setStatus(Value.kReverse);
	}
	
	public void setStatus(Value value) {
		lifter.set(value);
	}
	
	public Value getStatus() {
		return lifter.get();
	}
	
	public void reportStatus() {
		String report = "";
		Value status = getStatus();
		
		if (status == Value.kForward)
			report = "Up";
		else if (status == Value.kReverse)
			report = "Down";
		else
			report = "No Power";
		
		SmartDashboard.putString("Lifter Position", report);
	}
	
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}

