package org.usfirst.frc.team4320.robot.subsystems;

import org.usfirst.frc.team4320.robot.RobotMap;
import org.usfirst.frc.team4320.utils.Reportable;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 */
public class GearCatcher extends Subsystem implements Reportable {

	private DoubleSolenoid catcher;
	
	public GearCatcher() {
		catcher = new DoubleSolenoid(RobotMap.GEAR_CATCHER_OPEN, RobotMap.GEAR_CATCHER_CLOSE);
		setStatus(Value.kForward);
	}
	
	public void setStatus(Value value) {
		catcher.set(value);
	}
	
	public Value getStatus() {
		return catcher.get();
	}
	
	public void reportStatus() {
		String report = "";
		Value status = getStatus();
		
		if (status == Value.kForward)
			report = "Open";
		else if (status == Value.kReverse)
			report = "Closed";
		else
			report = "No Power";
		
		SmartDashboard.putString("Catching Grips", report);
	}

    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}

