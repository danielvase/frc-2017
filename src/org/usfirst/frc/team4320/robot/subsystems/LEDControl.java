package org.usfirst.frc.team4320.robot.subsystems;

import org.usfirst.frc.team4320.robot.RobotMap;

import edu.wpi.first.wpilibj.Relay;
import edu.wpi.first.wpilibj.Relay.Value;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 */
public class LEDControl extends Subsystem {

	private Relay LEDController;
	
	public LEDControl() {
		LEDController = new Relay(RobotMap.LED_SPIKE);
	}
	
	public void setStatus(Value value) {
		LEDController.set(value);
	}
	
	public Value getStatus() {
		return LEDController.get();
	}

    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}

