package org.usfirst.frc.team4320.robot.subsystems;

import org.usfirst.frc.team4320.robot.RobotMap;
import org.usfirst.frc.team4320.robot.commands.TeleClimb;
import org.usfirst.frc.team4320.utils.Reportable;

import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Spark;

/**
 *
 */
public class ClimbingSubsystem extends Subsystem implements Reportable {

	private Spark climbers;
	private DigitalInput switch1, switch2;
	
	public ClimbingSubsystem() {
		switch1 = new DigitalInput(RobotMap.CLIMBER_SWITCH_1);
		switch2 = new DigitalInput(RobotMap.CLIMBER_SWITCH_2);
		climbers = new Spark(RobotMap.CIMBING_MOTORS);
		
		//climbers.setSafetyEnabled(true);
	}
	
	public void climb(double speed, boolean force) {
		if (speed > -0.1)
			speed = 0;
		
		if (!force && (switch1.get() || switch2.get()))
			speed = 0;
		
		climbers.set(speed);
	}
	
	public void reportStatus() {
		SmartDashboard.putBoolean("Climbing Switch Pressed", switch1.get() || switch2.get());
	}
	
    public void initDefaultCommand() {
    	setDefaultCommand(new TeleClimb());
    }
}

