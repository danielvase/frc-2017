package org.usfirst.frc.team4320.robot.subsystems;
 
import org.usfirst.frc.team4320.robot.Robot;
import org.usfirst.frc.team4320.robot.RobotMap;
import org.usfirst.frc.team4320.robot.commands.TeleDrive;
import org.usfirst.frc.team4320.utils.Reportable;

import com.ctre.CANTalon;
import com.ctre.CANTalon.FeedbackDevice;
import com.ctre.CANTalon.TalonControlMode;

import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.PIDSourceType;
 
/**
 *
 */
public class Chassis extends Subsystem implements Reportable {
 	
    private RobotDrive drive;
    private CANTalon leftSlave, leftMaster, rightSlave, rightMaster;
    private Encoder leftEncoder, rightEncoder;
    private final double p = 1, i = 0, d = 6;
   
    public Chassis() {
        leftSlave = new CANTalon(RobotMap.REAR_LEFT_DRIVE);
        leftMaster = new CANTalon(RobotMap.FRONT_LEFT_DRIVE);
        rightSlave = new CANTalon(RobotMap.REAR_RIGHT_DRIVE);
        rightMaster = new CANTalon(RobotMap.FRONT_RIGHT_DRIVE);
        
        leftSlave.changeControlMode(TalonControlMode.Follower);
        leftSlave.set(RobotMap.FRONT_LEFT_DRIVE);
        rightSlave.changeControlMode(TalonControlMode.Follower);
        rightSlave.set(RobotMap.FRONT_RIGHT_DRIVE);
       
        leftEncoder = new Encoder(RobotMap.LEFT_ENCODER_A, RobotMap.LEFT_ENCODER_B, false);
        rightEncoder = new Encoder(RobotMap.RIGHT_ENCODER_A, RobotMap.RIGHT_ENCODER_B, true);

        leftEncoder.setPIDSourceType(PIDSourceType.kDisplacement);
        rightEncoder.setPIDSourceType(PIDSourceType.kDisplacement);
        updateDistancePerPulse();
        
        /*
        leftMaster.setFeedbackDevice(FeedbackDevice.QuadEncoder);
        leftMaster.setPID(p, i, d);
        rightMaster.setFeedbackDevice(FeedbackDevice.QuadEncoder);
        rightMaster.setPID(p, i, d);
        */
        
       
        drive = new RobotDrive(leftMaster, rightMaster);
       
        // Safety
        leftSlave.setSafetyEnabled(true);
        leftMaster.setSafetyEnabled(true);
        rightSlave.setSafetyEnabled(true);
        rightMaster.setSafetyEnabled(true);
        drive.setSafetyEnabled(true);
    }
   
    public void teleDrive(double speed, double rotate) {
        if ((speed < 0.05 && speed >= 0) || (speed > -0.05 && speed <= 0))
            speed = 0;
        if ((rotate < 0.05 && rotate >= 0) || (rotate > -0.05 && rotate <= 0))
            rotate = 0;
       
        drive.arcadeDrive(speed, rotate);
    }
   
    public void driveByDistance(double distance, double time) {
        driveByDistance(distance, distance, time);
    }
   
    public void turnByDegrees(double angle, double time) {
    	double distance = RobotMap.WHEEL_CIRCUMFERENCE * angle / 360;
    	
    	driveByDistance(-distance, distance, time);
    }
    
    private void driveByDistance(double leftDistance, double rightDistance, double time) {
    	DriverStation.reportError("Not implemented", true);
    	return;
    }
   
    public void updateDistancePerPulse() {
        if (Robot.shifters == null)
            return;
        
        double distancePerPulse = Robot.shifters.getRatio();
        leftEncoder.setDistancePerPulse(distancePerPulse);
        rightEncoder.setDistancePerPulse(distancePerPulse);
       
        leftEncoder.reset();
        rightEncoder.reset();
    }
    
    public void reportStatus() {
    	SmartDashboard.putNumber("Left Encoder", leftEncoder.getDistance());
    	SmartDashboard.putNumber("Right Encoder", rightEncoder.getDistance());
    }
    
    public void initDefaultCommand() {
        setDefaultCommand(new TeleDrive());
    }
}