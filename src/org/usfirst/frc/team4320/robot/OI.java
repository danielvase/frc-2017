package org.usfirst.frc.team4320.robot;

import org.usfirst.frc.team4320.robot.commands.ToggleCompressor;
import org.usfirst.frc.team4320.robot.commands.ToggleGearCatcher;
import org.usfirst.frc.team4320.robot.commands.ToggleGearLifter;
import org.usfirst.frc.team4320.robot.commands.ToggleLED;
import org.usfirst.frc.team4320.robot.RobotMap;
import org.usfirst.frc.team4320.robot.commands.ChassisToPowerMode;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {

	private Joystick driveStick;
	private XboxController xboxStick;
	
	public OI() {
		driveStick = new Joystick(RobotMap.DRIVE_STICK);
		xboxStick = new XboxController(RobotMap.XBOX_STICK);
		
		JoystickButton triggerButton = new JoystickButton(driveStick, 1);
		JoystickButton catchButton = new JoystickButton(xboxStick, 4);
		JoystickButton liftButton = new JoystickButton(xboxStick, 3);
		JoystickButton compressorButton = new JoystickButton(xboxStick, 2);
		JoystickButton LEDButton = new JoystickButton(xboxStick, 1);
		
		triggerButton.whileHeld(new ChassisToPowerMode());
		catchButton.whenPressed(new ToggleGearCatcher());
		liftButton.whenPressed(new ToggleGearLifter());
		compressorButton.whenPressed(new ToggleCompressor());
		LEDButton.whenPressed(new ToggleLED());
	}
	
	public Joystick getDriveStick() {
		return driveStick;
	}
	
	public XboxController getXboxStick() {
		return xboxStick;
	}
}
