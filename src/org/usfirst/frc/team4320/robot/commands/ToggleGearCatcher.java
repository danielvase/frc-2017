package org.usfirst.frc.team4320.robot.commands;

import org.usfirst.frc.team4320.robot.Robot;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.command.InstantCommand;

/**
 *
 */
public class ToggleGearCatcher extends InstantCommand {

    public ToggleGearCatcher() {
        super();
        requires(Robot.gearCatcher);
    }

    // Called once when the command executes
    protected void initialize() {
    	Value value = Value.kOff;
    	
    	if (Robot.gearCatcher.getStatus() == Value.kForward)
    		value = Value.kReverse;
    	else
    		value = Value.kForward;
    	
    	Robot.gearCatcher.setStatus(value);
    }

}
