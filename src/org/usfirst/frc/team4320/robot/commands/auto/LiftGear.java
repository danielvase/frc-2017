package org.usfirst.frc.team4320.robot.commands.auto;

import org.usfirst.frc.team4320.robot.commands.SetGearCatcher;
import org.usfirst.frc.team4320.robot.commands.SetGearLifter;
import org.usfirst.frc.team4320.robot.commands.Wait;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.command.CommandGroup;

/**
 *
 */
public class LiftGear extends CommandGroup {

    public LiftGear() {
    	addSequential(new SetGearCatcher(Value.kReverse));
    	addSequential(new Wait(0.2));
    	addSequential(new SetGearLifter(Value.kForward));
    }
}
