package org.usfirst.frc.team4320.robot.commands.auto;

import org.usfirst.frc.team4320.robot.commands.AutoTimedDrive;
import org.usfirst.frc.team4320.robot.commands.AutoTimedDrive.Direction;
import org.usfirst.frc.team4320.robot.commands.SetGearCatcher;
import org.usfirst.frc.team4320.robot.commands.SetGearLifter;
import org.usfirst.frc.team4320.robot.commands.Wait;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.command.CommandGroup;

/**
 *
 */
public class ReleaseGear extends CommandGroup {

    public ReleaseGear() {
    	addSequential(new SetGearCatcher(Value.kForward));
    	addSequential(new SetGearLifter(Value.kReverse));
    	addSequential(new Wait(0.3));
    	addSequential(new AutoTimedDrive(Direction.kBackwards, 0.5, 1));
    }
}
