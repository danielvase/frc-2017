package org.usfirst.frc.team4320.robot.commands;

import org.usfirst.frc.team4320.robot.Robot;

import edu.wpi.first.wpilibj.command.InstantCommand;

/**
 *
 */
public class ToggleCompressor extends InstantCommand {

    public ToggleCompressor() {
        super();
        requires(Robot.pneumatics);
    }

    // Called once when the command executes
    protected void initialize() {
    	if (Robot.pneumatics.isOn())
    		Robot.pneumatics.stopCompressor();
    	else
    		Robot.pneumatics.startCompressor();
    }

}
