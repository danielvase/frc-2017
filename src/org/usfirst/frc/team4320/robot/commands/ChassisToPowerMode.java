package org.usfirst.frc.team4320.robot.commands;

import org.usfirst.frc.team4320.robot.Robot;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.InstantCommand;

/**
 *
 */
public class ChassisToPowerMode extends Command {

    public ChassisToPowerMode() {
        requires(Robot.shifters);
    }

    // Called once when the command executes
    protected void initialize() {
		Robot.shifters.setStatus(Value.kForward);
    }

	@Override
	protected boolean isFinished() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected void end() {
		Robot.shifters.setStatus(Value.kReverse);
	}
}
