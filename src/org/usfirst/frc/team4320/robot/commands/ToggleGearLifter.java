package org.usfirst.frc.team4320.robot.commands;

import org.usfirst.frc.team4320.robot.Robot;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.command.InstantCommand;

/**
 *
 */
public class ToggleGearLifter extends InstantCommand {

    public ToggleGearLifter() {
        super();
        requires(Robot.gearLifter);
    }

    // Called once when the command executes
    protected void initialize() {
    	Value value = Value.kOff;
    	
    	if (Robot.gearLifter.getStatus() == Value.kForward)
    		value = Value.kReverse;
    	else
    		value = Value.kForward;
    	
    	Robot.gearLifter.setStatus(value);
    }

}
