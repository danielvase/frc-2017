package org.usfirst.frc.team4320.robot.commands;

import org.usfirst.frc.team4320.robot.Robot;

import edu.wpi.first.wpilibj.Relay.Value;
import edu.wpi.first.wpilibj.command.InstantCommand;

/**
 *
 */
public class ToggleLED extends InstantCommand {

    public ToggleLED() {
        super();
        requires(Robot.LEDs);
    }

    // Called once when the command executes
    protected void initialize() {
    	Value value = Value.kOff;
    	
    	if (Robot.LEDs.getStatus() == Value.kOff)
    		value = Value.kOn;
    	
    	Robot.LEDs.setStatus(value);
    }

}
