package org.usfirst.frc.team4320.robot.commands;

import org.usfirst.frc.team4320.robot.Robot;

import edu.wpi.first.wpilibj.command.TimedCommand;

/**
 *
 */
public class AutoTimedDrive extends TimedCommand {

	public enum Direction {
		kBackwards,
		kForwards
	}
	
	private double speed;
	
    public AutoTimedDrive(Direction direction, double speed, double timeout) {
        super(timeout);
        this.speed = direction == Direction.kBackwards ? -Math.abs(speed) : Math.abs(speed);
        requires(Robot.chassis);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	Robot.chassis.teleDrive(speed, 0);
    }

    // Called once after timeout
    protected void end() {
    	Robot.chassis.teleDrive(0, 0);
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
