package org.usfirst.frc.team4320.robot.commands;

import org.usfirst.frc.team4320.robot.Robot;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.command.InstantCommand;

/**
 *
 */
public class SetGearLifter extends InstantCommand {

	private Value value;
	
    public SetGearLifter(Value value) {
        super();
        this.value = value;
        requires(Robot.gearLifter);
    }

    // Called once when the command executes
    protected void initialize() {
    	Robot.gearLifter.setStatus(value);
    }

}
