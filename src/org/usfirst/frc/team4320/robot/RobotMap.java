package org.usfirst.frc.team4320.robot;
 
/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
 
    // Chassis - CAN
    public static final int REAR_LEFT_DRIVE = 1;
    public static final int FRONT_LEFT_DRIVE = 2;
    public static final int REAR_RIGHT_DRIVE = 3;
    public static final int FRONT_RIGHT_DRIVE = 4;
   
    // Chassis encoders - DIO
    public static final int LEFT_ENCODER_A = 0;
    public static final int LEFT_ENCODER_B = 1;
    public static final int RIGHT_ENCODER_A = 2;
    public static final int RIGHT_ENCODER_B = 3;
   
    // Gear Shifters - PCM
    public static final int SHIFTER_LEFT_OPEN = 0;
    public static final int SHIFTER_LEFT_CLOSE = 1;
    public static final int SHIFTER_RIGHT_OPEN = 2;
    public static final int SHIFTER_RIGHT_CLOSE = 3;
   
    // Climbing motors - PWM
    public static final int CIMBING_MOTORS = 0;
   
    // Climbing switches - DIO
    public static final int CLIMBER_SWITCH_1 = 4;
    public static final int CLIMBER_SWITCH_2 = 5;
   
    // Gear catcher - PCM
    public static final int GEAR_CATCHER_OPEN = 4;
    public static final int GEAR_CATCHER_CLOSE = 5;
   
    // Gear lifter - PCM
    public static final int GEAR_LIFTER_OPEN = 6;
    public static final int GEAR_LIFTER_CLOSE = 7;
   
    // LED Control - Relay
    public static final int LED_SPIKE = 0;
 
    // General Pneumatics - Analog In
    public static final int PRESSURE_SENSOR = 0;
    
    // OI
    public static final int DRIVE_STICK = 1;
    public static final int XBOX_STICK = 2;
   
    // Misc
    public static final double WHEEL_DIAMETER = 6 * 2.54;
    public static final double WHEEL_CIRCUMFERENCE = WHEEL_DIAMETER * Math.PI;
    public static final double LOW_SHIFTER_RATIO = 6.601;
    public static final double HIGH_SHIFTER_RATIO = 14.999;
    public static final double SHIFTER_ENCODER_RATIO = 3;
    public static final double ENCODER_STEPS_PER_REVOLUTION = 256;
    public static final double LOW_RATIO_DISTANCE_PER_PULSE = WHEEL_CIRCUMFERENCE * LOW_SHIFTER_RATIO * SHIFTER_ENCODER_RATIO / ENCODER_STEPS_PER_REVOLUTION;
    public static final double HIGH_RATIO_DISTANCE_PER_PULSE = WHEEL_CIRCUMFERENCE * HIGH_SHIFTER_RATIO * SHIFTER_ENCODER_RATIO / ENCODER_STEPS_PER_REVOLUTION;  
}