package org.usfirst.frc.team4320.utils;

public interface Reportable {

	public void reportStatus();
}
